<?php
mb_internal_encoding('UTF-8');

require __DIR__ . '/vendor/autoload.php';

const BASE_DIR = __DIR__;

// ukazka pouziti twig knihovny
/*echo $twig->render('test.html.twig', [
    'title' => 'My title',
    'test' => 'Hello test!',
]);*/

const base_dir = 'C:/xampp/htdocs/';

$pdo = new PDO('mysql:host=localhost;dbname=mvc_db', 'root', '');

$loader = new \Twig\Loader\FilesystemLoader(BASE_DIR . '/src/templates');
$twig = new \Twig\Environment($loader/*, ['cache' => BASE_DIR . '/var/cache/twig/compilation_cache',]*/);

function autoload($class)
{
    $base_dir = __DIR__ . '/src/';
    $file = $base_dir . str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
}

spl_autoload_register('autoload');

$router = new Router($pdo, $twig);
$router->route();

