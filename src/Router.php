<?php


use User\Login\Controller;
use User\UserLogin;

class Router
{
    private PDO $pdo;
    private array $routes = [
        'article/error' => ['view' => 'Article\\Error\\View', 'template' => 'missing']
    ];
    private Twig\Environment $twig;

    public function __construct($pdo, $twig)
    {
        $this->pdo = $pdo;
        $this->twig = $twig;
    }

    public function route()
    {
        $login = new UserLogin($this->pdo, []);
        $loginController = new Controller();
        $loginController->checkSession($login);

        //rozeber routu na jednotlivé části
        $route = explode('/', ltrim(explode('?', $_SERVER['REQUEST_URI'])[0], '/'));
        //připrav parametry metody
        $params = array_slice($route, 3);

        if (isset($route[0]) && isset($route[1]) && isset($this->routes[$route[0] . '/' . $route[1]])) //routuj staticky pokud je routa
        {
            $triad = $this->routes[$route[0] . '/' . $route[1]];
            $model = isset($triad['model']) ? new $triad['model']() : null;
            $view = isset($triad['view']) ? new $triad['view']($this->twig, $triad['template']) : null;
            $controller = isset($triad['controller']) ? new $triad['controller']() : null;
            if (!empty($controller) && !empty($model) && isset($triad['method'])) {
                $model = $controller->{$triad['method']}($model, $_REQUEST);
            }
        } else //routuj dynamicky
        {
            //pokud jsi na rootu vykresli výpis článků
            if (empty($route[0])) {
                $route = ['article', 'all'];
            }
            //slož cestu ke třídě a jednotlivé třídy
            $path = ucwords($route[0]) . '\\' . ucwords($route[1]) . '\\';
            $modelClass = ucwords($route[0]) . '\\' . ucwords($route[0]) . ucwords($route[1]);
            $viewClass = $path . 'View';
            $controllerClass = $path . 'Controller';
            //vyber metodu
            if (isset($route[2]))
                $method = $route[2];

            //instancuj triádu
            $view = new $viewClass($this->twig, $route[1]);
            $model = new $modelClass($this->pdo, $params);
            $controller = new $controllerClass();
            //pokud metoda existuje, zavolej ji
            if (!empty($method)) {
                $model = $controller->{$method}($model, array_merge($params, $_REQUEST));
            }
        }
        $view->output($model);
    }

}