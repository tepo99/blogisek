-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: mvc_db
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `content` text COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'První','První článek\r\n\r\nToto je první článek webu','První článek webu'),(2,'Druhý','Druhý článek\r\n\r\nToto je druhý článek webu','Druhý článek webu'),(13,'Čtvrtý','blablabla','Toto je čtvrtý článek'),(17,'Třetí','aoeuaoeuaoeuaoeuaou','aoeuaoeu'),(18,'Pátý','oaeuaoeuaoeu','aoeu');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_comment`
--

DROP TABLE IF EXISTS `article_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_comment` (
  `article_id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `insert_date` mediumtext COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `article_comment_article_article_id_fk` (`article_id`),
  CONSTRAINT `article_comment_article_article_id_fk` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_comment`
--

LOCK TABLES `article_comment` WRITE;
/*!40000 ALTER TABLE `article_comment` DISABLE KEYS */;
INSERT INTO `article_comment` VALUES (13,'admin','První komentář',11,'1597906127'),(13,'dominik-spilka','Druhý komentář',12,'1597906136'),(2,'test','<script>alert(\'test\');</script>',13,'1598278194'),(13,'admin','aoeuaoeu',14,'1598341837'),(13,'admin','aoeuaoeuaouaoeu',15,'1598342577'),(2,'admin','<strong>ahoj</strong> světe.',16,'1598344270'),(2,'admin','aoeuaoeuaoeu',17,'1598345767'),(2,'aaoeuaoeu','aaoeuaoeu',18,'1598347046'),(2,'dominik-spilka','aoeuaoeu',19,'1598349192'),(2,'admin','aoeuaoeuaoeuaoeu',20,'1598349212'),(13,'admin','aoeuoaeu',21,'1598350556'),(13,'jára','aoeuaoeu',22,'1598350557'),(13,'aoeu','aoeuaoeu',23,'1598351616'),(13,'oeuoeu','oeuoeuoeu',24,'1598351657'),(13,'admin','aoeuaoeu',25,'1598351679'),(18,'admin','6465454',28,'1598357494'),(13,'admin','aoeuaoeu',29,'1598425614'),(13,'dominik-spilka','oeeuoaoeu',30,'1598425775'),(13,'dominik-spilka','oeeuoaoeu',31,'1598425842'),(13,'dominik-spilka','oeuaaoeuaoeu',32,'1598425872'),(13,'admin','aoeuaou',33,'1598426070');
/*!40000 ALTER TABLE `article_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `sessionid` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `expiry_time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `name` (`username`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'admin','597f5441e7d174b607873874ed54b974674986ad543e7458e28a038671c9f64c','39cf2d17a47b4eb853fe9d40901fd03878fc640944b287e1dd6553448f499bc3',1600949262);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-27  8:44:09
