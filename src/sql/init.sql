use mvc_db;

create table if not exists article
(
    article_id  int auto_increment
        primary key,
    title       varchar(255) null,
    content     text         null,
    description varchar(255) null
);

create table if not exists article_comment
(
    article_id  int          not null,
    username    varchar(50)  not null,
    content     varchar(255) not null,
    comment_id  int auto_increment
        primary key,
    insert_date mediumtext   not null,
    constraint article_comment_article_article_id_fk
        foreign key (article_id) references article (article_id)
            on delete cascade
);

create table if not exists user
(
    user_id     int auto_increment
        primary key,
    username    varchar(255) not null,
    password    varchar(255) not null,
    sessionid   varchar(255) null,
    expiry_time bigint       null,
    constraint name
        unique (username),
    constraint username
        unique (username)
);

