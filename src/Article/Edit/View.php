<?php

namespace Article\Edit;

use Article\AbstractView;
use Article\ArticleEdit;
use Article\Components\Form;
use Article\Components\Input;
use Article\Components\TextArea;
use Twig\Environment;

class View extends AbstractView
{
    public function __construct(Environment $twig, string $template)
    {
        $this->twig = $twig;
        $this->template = $template;
    }

    public function output(ArticleEdit $model)
    {
        $article = !empty($model->getArticle()) ? $model->getArticle() : ['title' => '', 'description' => '', 'content' => ''];
        $title = $article['title'] !== '' ? $article['title'] . ' - Upravit' : 'Přidat nový';
        $action = '/article/edit/submit/';
        if (isset($article['article_id'])) {
            $action .= $article['article_id'];
        }
        $editForm = new Form($action, 'post');
        $editForm->addField(new Input('Nadpis:', 'text', 'title', $article['title']));
        $editForm->addField(new TextArea('Popis:', 'description', $article['description']));
        $editForm->addField(new TextArea('Obsah:', 'content', $article['content']));
        $this->renderSelf(['form' => $editForm->build(), 'title' => $title, 'errors' => $model->getErrors()]);
    }
}