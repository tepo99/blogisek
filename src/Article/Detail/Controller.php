<?php

namespace Article\Detail;

use Article\ArticleDetail;

class Controller
{
    public function view(ArticleDetail $articleDetail): ArticleDetail
    {
        if (empty($articleDetail->getArticle())) {
            header('Location: /article/error');
        }
        return $articleDetail;
    }

    public function comment(ArticleDetail $articleDetail, $params): ArticleDetail
    {
        unset($params['submit']);
        unset($params[0]);
        $articleDetail = $articleDetail->addComment($params);
        if (empty($articleDetail->getErrors()))
            header('Location: /article/detail/view/' . $params['article_id']);
        return $articleDetail;
    }

    public function comments(ArticleDetail $articleDetail): ArticleDetail
    {
        var_export($articleDetail->getComments());
        return $articleDetail;
    }
}