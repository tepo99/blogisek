<?php

namespace Article\Detail;

use Article\AbstractView;
use Article\ArticleDetail;
use Article\Components\Form;
use Article\Components\Input;
use Article\Components\TextArea;

class View extends AbstractView
{
    public function __construct(\Twig\Environment $twig, string $template)
    {
        $this->twig = $twig;
        $this->template = $template;
    }

    public function output(ArticleDetail $model): void
    {
        $article = $model->getArticle();
        $errors = $model->getErrors();
        if (!empty($errors)) {
            extract($errors);
        }
        extract($article);
        $action = '/article/detail/comment/';
        $action .= $article['article_id'];
        $commentForm = new Form('', '', 'submitArticle(' . $article['article_id'] . ')');
        $commentForm->addField(new Input('Jméno:', 'text', 'username', ''));
        $commentForm->addField(new TextArea('Komentář:', 'content', ''));
        $commentForm->addField(new Input('', 'hidden', 'article_id', $article['article_id']));
        $article['comment_form'] = $commentForm->build();
        $this->renderSelf($article);
    }
}