<?php


namespace Article\Comments;


use Article\AbstractView;
use Article\ArticleComments;
use Twig\Environment;

class View extends AbstractView
{

    public function __construct(Environment $twig, string $template)
    {
        $this->twig = $twig;
        $this->template = $template;
    }

    public function output(ArticleComments $articleComments)
    {
        $comments = $articleComments->getComments();
        for ($i = 0; $i < sizeof($comments); $i++) {
            $comments[$i]['content'] = htmlspecialchars($comments[$i]['content']);
            $comments[$i]['insert_date'] = date('j. n. Y H:i', $comments[$i]['insert_date']);
        }
        $this->renderSelf(['comments' => $comments]);
    }
}