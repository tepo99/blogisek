<?php


namespace Article\Components;


interface Validator
{
    public function validateField($value);
}