<?php


namespace Article\Components;


class TextArea implements Field
{

    private string $label;
    private string $name;
    private string $value;

    public function __construct($label, $name, $value = '')
    {
        $this->label = $label;
        $this->name = $name;
        $this->value = $value;
    }

    public function build()
    {
        return "<label for='$this->name' class='fullwidth'>$this->label</label><textarea id='$this->name' name='$this->name' class='fullwidth'>$this->value</textarea>";
    }
}