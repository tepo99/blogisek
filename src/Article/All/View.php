<?php

namespace Article\All;

use Article\AbstractView;
use Article\ArticleAll;
use Twig\Environment;

class View extends AbstractView
{
    public function __construct(Environment $twig, string $template)
    {
        $this->twig = $twig;
        $this->template = $template;
    }

    public function output(ArticleAll $model): void
    {
        $articles = $model->getArticles();
        if ($this->template) {
            $this->renderSelf(['articles' => $articles]);
        }
    }
}