<?php

namespace Article\Error;

use Article\AbstractView;
use Twig\Environment;

class View extends AbstractView
{
    public function __construct(Environment $twig, string $template)
    {
        $this->twig = $twig;
        $this->template = $template;
    }

    public function output($model)
    {
        header('HTTP/1.0 404 Not Found');
        $this->renderSelf([]);
    }
}