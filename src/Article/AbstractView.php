<?php


namespace Article;


use support\TwigLoader;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

abstract class AbstractView
{
    protected string $template;
    protected \Twig\Environment $twig;

    protected function renderSelf($extracts)
    {
        $extracts['sessionid'] = $this->isLoggedIn();
        $extracts['username'] = isset($_COOKIE['username']) ? $_COOKIE['username'] : null;
        try {
            echo $this->twig->render($this->template . '.html.twig', $extracts);
        } catch (LoaderError $e) {
            echo($e);
        } catch (RuntimeError $e) {
            echo($e);
        } catch (SyntaxError $e) {
            echo($e);
        }
    }

    public function isLoggedIn()
    {
        return isset($_COOKIE['sessionid']);
    }
}