<?php


namespace User;


use PDO;

class UserLogin
{
    private PDO $pdo;
    private array $errors = [];

    public function __construct(PDO $pdo, $params)
    {
        $this->pdo = $pdo;
        if (isset($params['errors'])) {
            $this->errors = $params['errors'];
        }
    }

    public function login($args): array
    {
        if (empty($args)) {
            return [];
        }
        $password = filter_var($args['password'], FILTER_SANITIZE_STRING);
        $username = filter_var($args['username'], FILTER_SANITIZE_STRING);
        return $this->createSession($username, $password);
    }

    private function createSession($username, $password): array
    {
        $hash = hash('sha256', $password . $username);
        $query = $this->pdo->prepare('select * from user where username = :username and password = :password');
        $query->execute(['username' => $username, 'password' => $hash]);
        $user = $query->fetch();
        if ($user === false) {
            $this->errors[] = 'Nesprávné jméno nebo heslo';
            return [];
        }
        $sessionID = hash('sha256', $username . $hash . time());
        $expiryTime = time() + 86400 * 30;
        $query = $this->pdo->prepare('update user set sessionid = :sessionid,  expiry_time = :expiry_time where user_id = :user_id');
        $response = $query->execute(['sessionid' => $sessionID, 'user_id' => $user['user_id'], 'expiry_time' => $expiryTime]);
        if ($response === false) {
            if (empty($errors)) {
                $this->errors[] = 'Nesprávné jméno nebo heslo.';
            }
            return [];
        }
        $user['sessionid'] = $sessionID;
        return $user;
    }

    public function checkSession($cookie): array
    {
        $this->clearSessions();
        $query = $this->pdo->prepare('select username from user where sessionid = :sessionid');
        $query->execute(['sessionid' => $cookie]);
        $user = $query->fetch();
        if ($user !== false) {
            return $user;
        }
        return [];
    }

    /**
     * Tohle je prasárna.
     * Kdyby to mělo být někde čištěno, tak by starý sessiony mazal cron.
     */
    private function clearSessions()
    {
        $query = $this->pdo->prepare('update user set sessionid = null, expiry_time = null where expiry_time < :current_time');
        $query->execute(['current_time' => time()]);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function logout($sessionid)
    {
        $query = $this->pdo->prepare('update user set sessionid =  null, expiry_time = null where sessionid  = :sessionid');
        $query->execute(['sessionid' => $sessionid]);
    }
}