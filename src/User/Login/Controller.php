<?php

namespace User\Login;

use User\UserLogin;

class Controller
{
    public function checkSession(UserLogin $userLogin): UserLogin
    {
        if (isset($_COOKIE['sessionid'])) {
            $sessionID = filter_var($_COOKIE['sessionid'], FILTER_SANITIZE_STRING);
            $user = $userLogin->checkSession($sessionID);
        }
        if (empty($user)) {
            $this->clearCookies();
        }
        return $userLogin;
    }

    private function clearCookies(): void
    {
        setcookie('sessionid', '', time() - 60, '/');
        setcookie('username', '', time() - 60, '/');
    }

    public function submit(UserLogin $userLogin): UserLogin
    {
        $user = $userLogin->login($_POST);
        if (!empty($user)) {
            setcookie('username', $user['username'], time() + 84300 * 30, '/');
            setcookie('sessionid', $user['sessionid'], time() + 84300 * 30, '/');
            header('location: /');
        }
        return $userLogin;
    }

    public function logout(UserLogin $userLogin): UserLogin
    {
        $sessionID = filter_var($_COOKIE['sessionid'], FILTER_SANITIZE_STRING);
        $userLogin->logout($sessionID);
        $this->clearCookies();
        header('location: /');
        return $userLogin;
    }
}