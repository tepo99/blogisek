<?php

namespace User\Login;

use Article\AbstractView;
use Article\Components\Form;
use Article\Components\Input;
use Twig\Environment;
use User\UserLogin;

class View extends AbstractView
{
    public function __construct(Environment $twig, string $template)
    {
        $this->twig = $twig;
        $this->template = $template;
    }

    public function output(UserLogin $userLogin)
    {
        $this->template = 'login';
        $loginForm = new Form('/user/login/submit', 'POST');
        $loginForm->addField(new Input('Uživatelské jméno', 'text', 'username'));
        $loginForm->addField(new Input('Heslo', 'password', 'password'));
        $this->renderSelf(['form' => $loginForm->build(), 'errors' => $userLogin->getErrors()]);
    }
}